Describe 'stu.sh'
  test_default_template='template'

  Include ./stu.sh

  Describe 'stu__template()'
    It 't1 fails because no argument is provided'
      When call stu__process_file_template
      The status should be failure
      The stderr should equal "ERROR: missing src_path argument"
    End
    It 't2 fails because wrong file path provided as arg'
      When call stu__process_file_template inexistentfile
      The status should be failure
      The stderr should equal "ERROR: inexistentfile template not found"
    End
    It 't3 fails because file path is correct but there is no extension'
      test_src_file='Makefile'
      # TODO shellspec: use directives %= and %text -> src https://github.com/shellspec/shellspec#puts---putsn----output-a-string-with-newline
      test_err_msg="$(cat <<END
ERROR: expected as input: ${test_src_file}.${test_default_template}, but got: ${test_src_file}.
You can change tmplext env var to whatever you need. Now tmplext=${test_default_template}
END
)"
      When call stu__process_file_template ${test_src_file}
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 't4 succeeds because there is correct file path with.template extension'
      test_src_file="spec_files/t4-example.template"
      When call stu__process_file_template "${test_src_file}"
      The status should be success
      The stdout should equal "${test_src_file%.${test_default_template}}"
    End
    It 't5 fails because its file has an incorrect TEMPLATE var'
      When call stu__process_file_template "spec_files/t5-incorrect_template_var.template"
      The status should be failure
      The stderr should equal "ERROR: [precheck] t5-incorrect_template_var.template is not defining TEMPLATE var correctly"
    End
    It 't6 fails because contains a not defined var inside'
      test_err_msg="$(cat <<END
ERROR: template t6-unknown_var.template contains variable(s) not defined.
details:
/bin/sh: 6: spec_files/t6-unknown_var.template: unknown: parameter not set
END
)"
      When call stu__process_file_template "spec_files/t6-unknown_var.template"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 't11 fails because the template extension does not match in the file'
      tmplext='fakext'
      test_src_file_path='spec_files/t4-example.template'
      test_src_file="$(basename "${test_src_file_path}")"
      test_err_msg="$(cat <<END
ERROR: expected as input: ${test_src_file}.${tmplext}, but got: ${test_src_file}.
You can change tmplext env var to whatever you need. Now tmplext=${tmplext}
END
)"
      When call stu__process_file_template "${test_src_file_path}"
      The status should be failure
      The stderr should equal "${test_err_msg}"
    End
    It 't12 fails because the file is not a template'
      test_dst_path="spec_files/t12-not_a_template.template"
      test_dst="$(basename "${test_dst_path}")"
      When call stu__process_file_template "${test_dst_path}"
      The status should be failure
      The stderr should equal "ERROR: [precheck] ${test_dst} is not defining TEMPLATE var correctly"
    End
    It 't13 suceeds with no files generated (test RM_SRC false)'
      DRY_TEST='false'
      RM_SRC='false'
      test_src_path="spec_files/ephemeral_ghost_file.template"
      test_dst_path="${test_src_path%.template}"
      cat > "${test_src_path}" <<EOF
#!/bin/sh
TEMPLATE="\$(cat <<END
this is a test
END
)"
EOF
      after_call() {
        # removes the two files generated in this test
        rm "${test_src_path}"
        rm "${test_dst_path}"
      }
      AfterCall after_call
      When call stu__process_file_template "${test_src_path}"
      The status should be success
      The stdout should equal "${test_src_path%.${test_default_template}}"
    End
    It 't14 suceeds with no files generated (test RM_SRC true)'
      DRY_TEST='false'
      RM_SRC='true'
      test_src_path="spec_files/ephemeral_ghost_file.template"
      test_dst_path="${test_src_path%.template}"
      cat > "${test_src_path}" <<EOF
#!/bin/sh
TEMPLATE="\$(cat <<END
this is a test
END
)"
EOF
      after_call() {
        # only removes dst file, because src file was removed because RM_SRC option is true
        rm "${test_dst_path}"
      }
      AfterCall after_call
      When call stu__process_file_template "${test_src_path}"
      The status should be success
      The stdout should equal "${test_src_path%.${test_default_template}}"
    End
  End

  Describe 'stu__templates()'
    It 't15 suceeds processing multiple templates on a directory'
      test_src_path="spec_files/t15-dir_example"
      test_stdout="$(cat <<END
spec_files/t15-dir_example/t15-template3
spec_files/t15-dir_example/t15-template2
spec_files/t15-dir_example/t15-dir_example-level2/t15-dir_example_level3/t15-template
END
)"
      When call stu__templates "${test_src_path}"
      The status should be success
      The stdout should equal "${test_stdout}"
    End
    It 't16 fails because no argument is provided'
      When call stu__templates
      The status should be failure
      The stderr should equal "ERROR: missing src_path argument"
    End
    It 't17 fails because directory is non existent'
      test_src_path='spec_files/this_does_not_exists'
      When call stu__templates "${test_src_path}"
      The status should be failure
      The stderr should equal "ERROR: ${test_src_path} directory not found"
    End
    It 't18 succeeds but no templates were processed on that directory'
      tmplext='fakext'
      test_src_path="spec_files/t15-dir_example"
      When call stu__templates "${test_src_path}"
      The status should be success
    End
  End
End
