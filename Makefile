# this seems to substitute just fine the old rake command, fine
all:
	conf.d/3000-network.sh

test:
	./test-with-shellcheck.sh
	shellspec --env DRY_TEST=true

.PHONY: coverage
# TODO fix coverage
coverage:
	shellspec --env DRY_TEST=true --kcov --kcov-options "--include-pattern=conf.d/0000-tembalib.sh"
