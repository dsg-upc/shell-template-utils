# shell-template-utils

generic utils for templates with posix shell

## launch tests

code is tested with [shellcheck](https://www.shellcheck.net/) and [shellspec](https://shellspec.info/)

`make test`
